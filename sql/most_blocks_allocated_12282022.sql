-- return the highest number of blocks ever allocated 
-- to each table by column. Utilize to determine which
-- large tables to target for query or space optimization
-- if possible

--https://docs.aws.amazon.com/redshift/latest/dg/r_SVV_DISKUSAGE.html

select db_id, 
    trim(name) as tablename, 
    col, 
    tbl, 
    max(blocknum)
from svv_diskusage
group by db_id, name, col, tbl
order by db_id, name, col, tbl;