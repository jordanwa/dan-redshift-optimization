-- return an estimated reduction in size of column for all 
-- tables if proper encoding and compression is used

--https://docs.aws.amazon.com/redshift/latest/dg/r_ANALYZE_COMPRESSION.html
--https://aws.amazon.com/about-aws/whats-new/2021/08/amazon-redshift-automatic-table-optimization-column-compression-encoding/

analyze compression