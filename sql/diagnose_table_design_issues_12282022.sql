-- return distribution style, sortkey, sortkey skew,
-- and row skew of each column in each table. Skew can
-- cause some nodes to perform more work than others,
-- leading to imbalanced worksloads and potentially
-- slower query execution times. Utilize to diagnose 
-- and address table design issues that could be 
-- caused by improper sort key choice

-- https://docs.aws.amazon.com/redshift/latest/dg/r_SVV_TABLE_INFO.html

select "schema",
    "table", 
    encoded, 
    diststyle, 
    sortkey1, 
    skew_sortkey1, 
    skew_rows
from svv_table_info
order by 1;