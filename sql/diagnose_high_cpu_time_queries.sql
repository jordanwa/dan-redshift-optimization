-- return queries using the most CPU time
-- NOTE: CPU time is distinct from query run time

--https://docs.aws.amazon.com/redshift/latest/dg/r_SVL_QUERY_METRICS_SUMMARY.html
--https://docs.aws.amazon.com/redshift/latest/dg/r_STL_QUERY.html

select qm.query as query_id, 
    trim(sq.querytxt) as sql_query, 
    qm.query_cpu_time, 
    qm.query_blocks_read, 
    qm.query_execution_time, 
    qm.query_cpu_usage_percent
from SVL_QUERY_METRICS_SUMMARY qm
left join stl_query sq on qm.query = sq.query
where qm.query_cpu_time > 0
order by qm.query_cpu_time desc