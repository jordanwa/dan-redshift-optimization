# Redshift Optimizaiton Queries
The SQL files stored here are some pertinent queries utilized to diagnose some issues, and brainstorm optimizations that could be made to the system

## SQL Files
* `analyze_compression_benefit`
  
  Return an estimated reduction in size of column for all tables if proper encoding and compression is used

* `automatic_table_optimization_recommendations`
  
  Return the recommended settings for performance optimization from Amazon Redshift Advisor for every table in each database

* `column_size_by_table`
  
  Return the size of each column for each table. Utilize for analysis and targeting of largested table/column
  
* `diagnose_high_block_reading_queries`
  
  Return queries reading the most 1 MB blocks per query
  
* `diagnose_high_cpu_time_queries`
  
  Return queries using the most CPU time (**Note**: CPU time is distinct from query run time)

* `diagnose_table_design_issues`

  Return distribution style, sortkey, sortkey skew, and row skew of each column in each table. Skew can cause some nodes to perform more work than others, leading to imbalanced worksloads and potentially slower query execution times. Utilize this information to diagnose and address table design issues that could be caused by improper sort key choice

* `most_blocks_allocated`

  Return the highest number of blocks ever allocated to each table by column. Utilize to determine which large tables to target for query or space optimization if possible

* `queries_on_tables_without_statistics`
  
  Return a count of queries running against tables that are missing statistics

* `tables_without_statistics`
  
  Return the schema name and table name of all tables with missing statistics

* `vacuum_sort_benefit`
    
  Return the impact of sorting a table by manually running VACUUM SORT