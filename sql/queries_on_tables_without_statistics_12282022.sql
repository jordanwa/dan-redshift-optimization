-- return a count of queries running against tables that are missing statistics

--https://docs.aws.amazon.com/redshift/latest/dg/r_STL_EXPLAIN.html 
--https://docs.aws.amazon.com/redshift/latest/dg/diagnostic-queries-for-query-tuning.html#identify-tables-with-missing-statistics

SELECT substring(trim(plannode),1,100) AS plannode,
       COUNT(*)
FROM stl_explain
WHERE plannode LIKE '%missing statistics%'
AND plannode NOT LIKE '%redshift_auto_health_check_%' --ignore pg_internal auto health checks
GROUP BY plannode
ORDER BY 2 DESC;