-- return the size of each column for each table.
-- Utilize for analysis and targeting of largested
-- table/column optimization

--https://docs.aws.amazon.com/redshift/latest/dg/r_SVV_DISKUSAGE.html

SELECT
  TRIM(name) as table_name,
  TRIM(pg_attribute.attname) AS column_name,
  COUNT(1) AS size
FROM svv_diskusage 
JOIN pg_attribute 
ON svv_diskusage.col = pg_attribute.attnum-1 
AND svv_diskusage.tbl = pg_attribute.attrelid
GROUP BY 1, 2