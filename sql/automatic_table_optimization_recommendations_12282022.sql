-- return the recommended settings for performance optimization
-- from Amazon Redshift Advisor for every table in each database

--https://docs.aws.amazon.com/redshift/latest/dg/t_Creating_tables.html#ato-monitoring-actions

select type, 
    database, 
    table_id, 
    group_id, 
    ddl, 
    auto_eligible 
from svv_alter_table_recommendations;