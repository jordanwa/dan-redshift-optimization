-- return the impact of sorting a table by manuallying running VACUUM SORT

--https://docs.aws.amazon.com/redshift/latest/dg/t_Reclaiming_storage_space202.html?shortFooter=true
--https://docs.aws.amazon.com/redshift/latest/dg/r_SVV_TABLE_INFO.html

select "database",
    "schema",
    "table", 
    unsorted,
    vacuum_sort_benefit,
    "size",
    pct_used
from svv_table_info 
order by 3;