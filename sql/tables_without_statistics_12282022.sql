-- return the schema name and table name of all tables with missing statistics

-- https://aws.amazon.com/blogs/big-data/top-10-performance-tuning-techniques-for-amazon-redshift

SELECT n.nspname AS schemaname, 
       c.relname AS tablename 
FROM pg_class c 
JOIN pg_namespace n ON n.oid = c. relnamespace 
JOIN (
       SELECT id FROM stv_tbl_perm  
       WHERE temp = 0 
       GROUP BY id 
       HAVING MAX (insert_pristine) > 0) t 
ON c.oid = t.id WHERE c.relkind = 'r' AND c.relowner >= 100 
AND c.oid NOT IN (
       SELECT DISTINCT starelid 
       FROM pg_statistic) 
ORDER BY schemaname, tablename;