# DAN-Redshift-Optimization
As part of the reporting effort for the DAN/Redshift upgrade plan, thorough analysis was conducted on our DEV environment redshift cluster prior to our Amazon CloudFormation template to Terraform migration. Findings from this analysis revealed various optimizations that could quickly be acted upon to improve system performance.

### Links
* [Jira Epic tracking this effort](https://cleardatadev.atlassian.net/browse/SEVEN-4144)
* [Documentation on findings from this effort](https://cleardatadocs.atlassian.net/wiki/spaces/DEV/pages/10703896900/DAN+Redshift+Upgrade+Planning)